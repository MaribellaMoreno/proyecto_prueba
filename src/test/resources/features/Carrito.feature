Feature: Agrego y retiro producto del carrito
  Yo como usuario de abstracta quiero agregar y remover producto del carrito de compra para cuando dude en comprarlo

  @Carrito
  Scenario: Agrego y remuevo producto del carrito de compras
    Given Ingreso a la web de OpenCart Abstracta
    When busco producto "iPhone" y selecciono primera opcion
    And agrego el producto al carrito de compras, ingreso y presiono View Cart
    And valido producto en carrito de compras
    #And remuevo el producto del carrito de compras
    #Then valido que el producto ya no se encuentre en carrito de compras

