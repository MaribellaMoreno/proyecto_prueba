package page;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class ProductoCarritoPage extends BasePage {

    public ProductoCarritoPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath="//a[contains(text(),'iPhone')]")
    private WebElement btnProducto;

    @FindBy(xpath="//button[contains(text(),'Add to Cart')]")
    private WebElement btnAddToCart;

    @FindBy(id="cart-total")
    private WebElement btnCarrito;

    @FindBy(xpath="//strong[contains(text(),' View Cart')]")
    private WebElement txtViewCart;

    @FindBy(xpath="(//td//a[contains(text(),'iPhone')])[2]")
    private WebElement txtProducto;

    @FindBy(xpath="(//i[@class='fa fa-times-circle']")
    private WebElement btnRemove;

    @FindBy(xpath="((//p[contains(text(),'Your shopping cart is empty!')])[2]]")
    private WebElement txtCartEmpty;


    public void clickProducto(){
        wait.until(ExpectedConditions.visibilityOf(btnProducto));
        btnProducto.click();
    }

    public void clickAddToCart(){
        wait.until(ExpectedConditions.visibilityOf(btnAddToCart));
        btnAddToCart.click();
    }

    public void clickCarrito(){
        wait.until(ExpectedConditions.visibilityOf(btnCarrito));
        btnCarrito.click();
    }

    public void clickViewCart(){
        wait.until(ExpectedConditions.visibilityOf(txtViewCart));
        txtViewCart.click();
    }

    public boolean validarProducto(){
        wait.until(ExpectedConditions.visibilityOf(txtProducto));
        return txtProducto.isDisplayed();
    }

    public void clickBtnRemove(){
        wait.until(ExpectedConditions.visibilityOf(btnRemove));
        btnRemove.click();
    }

    public boolean validarCartEmpty(){
        wait.until(ExpectedConditions.visibilityOf(txtCartEmpty));
        return txtCartEmpty.isDisplayed();
    }


}
