package page;

import base.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class HomePage extends BasePage {
    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }


    @FindBy(xpath="//input[@class='form-control input-lg']")
    private WebElement inputBusqueda;

    @FindBy(xpath="//button[@class='btn btn-default btn-lg']")
    private WebElement btnBuscar;



    public void SendtextProducto(String producto){
        wait.until(ExpectedConditions.visibilityOf(inputBusqueda));
        inputBusqueda.sendKeys(producto);
    }

    public void clickBuscar(){
        btnBuscar.click();
    }





}
