package step;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import page.HomePage;
import page.ProductoCarritoPage;

public class CarritoStep {
    HomePage homePage;
    ProductoCarritoPage productoCarritoPage;

    public CarritoStep(){
        homePage=new HomePage(Hooks.driver);
        productoCarritoPage=new ProductoCarritoPage(Hooks.driver);
    }

    @Given("Ingreso a la web de OpenCart Abstracta")
    public void ingresoALaWebDeOpenCartAbstracta() {
        Hooks.driver.get("http://opencart.abstracta.us/");
    }

    @When("busco producto {string} y selecciono primera opcion")
    public void buscoProductoYSeleccionoPrimeraOpcion(String producto) {
        homePage.SendtextProducto(producto);
        homePage.clickBuscar();
    }

    @And("agrego el producto al carrito de compras, ingreso y presiono View Cart")
    public void agregoElProductoAlCarritoDeComprasIngresoYPresiono() {
        productoCarritoPage.clickProducto();
        productoCarritoPage.clickAddToCart();
        productoCarritoPage.clickCarrito();
        productoCarritoPage.clickViewCart();
    }

    @And("valido producto en carrito de compras")
    public void validoProductoEnCarritoDeCompras() {
        Assert.assertTrue(productoCarritoPage.validarProducto());
    }

    @And("remuevo el producto del carrito de compras")
    public void remuevoElProductoDelCarritoDeCompras() {
        productoCarritoPage.clickBtnRemove();
    }

    @Then("valido que el producto ya no se encuentre en carrito de compras")
    public void validoQueElProductoYaNoSeEncuentreEnCarritoDeCompras() {
        Assert.assertTrue(productoCarritoPage.validarCartEmpty());

    }
}
